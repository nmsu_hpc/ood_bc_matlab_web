# OOD Interactive Matlab App

This is an Open Ondemand app designed to run the new web interface for MATLAB.

## Prerequisites

This app is designed to use software modules (Lmod) and currently does use containers.

- [MATLAB](https://www.mathworks.com/)
  - Version >= 2020b
  - For dependencies see [matlab-deps](https://github.com/mathworks-ref-arch/container-images/tree/master/matlab-deps)
- [MATLAB Proxy](https://github.com/mathworks/matlab-proxy)
  - Version >= 0.3.0
  - This is a seperate application by MATHWORKS but not shipped with MATLAB itself
  - Requires "X Virtual Frame Buffer (Xvfb)"

## Site Customizations

- `form.js`
  - Optional and can be removed.
- `form.yml.erb`
  - Values need to be updated to reflect your site.
  - Especially 'version' which contains the modules (space seperated) that will be loaded to access the desired version of matlab.
  - The dynamic partition list may or maynot work for you.
- `partitions.json`
  - Partition values for dynamic partition list in form.yml.erb -> form 'custom_queue'.
- `before.sh`
  - Update the host/HOST assignment to reflect your node names.  We use a '-ib0' to force the usage of specific network so this will break for most other people.
- `script.sh`
  - You'll need to adjust how you are getting MATLAB Proxy. We do this with a conda environment.
    - Adjust the `module load` and `conda activate`
- `submit.yml.erb`
  - Adjujst parameters to match the scheduler/settings at your site.
